<?php
$location = realpath(dirname(__FILE__));
require_once $location . '/ritchey_increment_items_i1_v2.php';
$array = array('');
$item_list = array('a','b','c');
$time_stamp = date_timestamp_get(date_create());
$alarm = $time_stamp + 60;
while (date_timestamp_get(date_create()) < $alarm){
	$array = ritchey_increment_items_i1_v2($array, $item_list, TRUE);
}
if ($array === FALSE){
	echo "FALSE\n";
} else {
	print_r($array);
	echo "\n";
}
?>